"""
CollectiveList.urls - CollectiveList URL Configuration
Copyright (C) 2020 >make Utopia_

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.contrib import admin
from django.urls import include, path
from CollectiveList import settings
from django.conf.urls.static import static
from . import views

admin.site.site_header = 'KollektivListe Administration'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/', include('accounts.urls')),
    path('about/', views.AboutPageView.as_view(), name='about'),
    path('impressum/', views.ImpressumPageView.as_view(), name='impressum'),
    path('kontakt/', views.ContactPageView.as_view(), name='kontakt'),
    path('newsletter/', views.NewsletterPageView.as_view(), name='newsletter'),
    path('links/', views.LinksPageView.as_view(), name='links'),
    path('tinymce/', include('tinymce.urls')),
    path('', include('collectives.urls')),
    path('', include('offers.urls')),
    path('', include('events.urls')),
    path('', views.HomePageView.as_view(), name='startseite')
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
