"""
collectives.signals - signal handler for collectives
Copyright (C) 2020 >make Utopia_

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.db.models.signals import pre_save
from django.dispatch import receiver
from .models import AdditionalAddress
from . import utils


@receiver(pre_save, sender=AdditionalAddress, dispatch_uid="address_save_receiver")
def add_geodata(sender, instance, **kwargs):
    geodata = utils.get_geodata_by_address(instance.address,
                                           instance.postal_code,
                                           instance.city)
    instance.lat = geodata.get("lat")
    instance.lon = geodata.get("lon")
