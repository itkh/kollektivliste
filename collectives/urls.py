"""
collectives.urls - url registration for collectives
Copyright (C) 2020 >make Utopia_

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.urls import path
from . import views

urlpatterns = [
    # path('', views.CollectiveListView.as_view(), name='startseite'),
    #path('about/', views.AboutPageView.as_view(), name='about'),
    #path('impressum/', views.ImpressumPageView.as_view(), name='impressum'),
    #path('kontakt/', views.ContactPageView.as_view(), name='kontakt'),
    path('collective/list/', views.CollectiveListView.as_view(),
         name='collective_list'),
    path('collective/new/', views.CollectiveCreateView.as_view(),
         name='collective_new'),
    path('collective/<int:pk>/', views.CollectiveDetailView.as_view(),
         name='collective_detail'),
    path('collective/<int:pk>/update/', views.CollectiveUpdateView.as_view(),
         name='collective_update'),
    path('collective/<int:pk>/check/', views.CollectiveCheckView.as_view(),
         name='collective_check'),
    path('collective/<int:pk>/delete/', views.CollectiveDeleteView.as_view(),
         name='collective_delete'),
    path('collective/map/', views.MapView.as_view(),
         name='collective_map'),
    path('collective/osm_api/', views.get_collectives_coordinates,
         name='collective_osm_api'),
]
