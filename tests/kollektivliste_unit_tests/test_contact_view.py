from CollectiveList.views import ContactPageView
from django.core import mail
from django.test import Client, RequestFactory, TestCase


class ContactViewTests(TestCase):

    def setUp(self):
        self.c = Client()

    def test_contact_view_get(self):
        resp = self.c.get("/kontakt/")
        self.assertEqual(resp.status_code, 200)
        content = resp.content.decode("utf-8")
        self.assertTrue("Nimm Kontakt mit uns auf" in content)
        self.assertFalse("erfolgreich versendet" in content)
        self.assertFalse("nicht versendet" in content)

    def test_contact_view_post_has_success_message(self):
        with self.settings(CONTACT_EMAIL_RECEIVER="user@example.local"):
            resp = self.c.post(
                "/kontakt/",
                {"sender": "vl@it.kollektiv.hamburg", "subject": "Test", "text": "Eine Nachricht"}
            )
        send_email = mail.outbox[0]

        self.assertEqual(resp.status_code, 200)
        content = resp.content.decode("utf-8")
        self.assertTrue("Kontaktanfrage" in content)
        self.assertTrue("erfolgreich versendet" in content)

        self.assertEqual(send_email.from_email, "vl@it.kollektiv.hamburg")
        self.assertEqual(send_email.to[0], "user@example.local")
        self.assertEqual(send_email.subject, "Test")
        self.assertEqual(send_email.body, "Eine Nachricht")

    def test_contact_view_post_has_error_msg_if_sender_not_email(self):
        with self.settings(EMAIL_RECEIVER="user@example.local"):
            resp = self.c.post(
                "/kontakt/",
                {"sender": "vlqkollektiv.hamburg", "subject": "Test", "text": "Eine Nachricht"}
            )
        self.assertEqual(len(mail.outbox), 0)
        self.assertEqual(resp.status_code, 200)
        content = resp.content.decode("utf-8")
        self.assertTrue("Anfrage" in content)
        self.assertTrue("nicht versendet" in content)
