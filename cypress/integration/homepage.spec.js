// https://docs.cypress.io/guides/references/best-practices.html
describe('Testing the Home Page', function () {
  beforeEach(function () {
    cy.visit('/')
  })

  it('Homepage has navigation entries', function() {
    cy.get('[data-cy=navigation-list]').children().should('have.length', 6)
    cy.get('[data-cy=navigation-list]').children().eq(-2).should('contain', 'Newsletter')
  })

  it('Homepage has main headline and cards', function() {
    cy.get('[data-cy=main-headline]').should('contain', 'Alles rund um Kollektivbetriebe')
    // cy.get('[data-cy=events-card]').should('contain', 'Termine (eine Auswahl)')
    // cy.get('[data-cy=events-card]').should('contain', 'Übersicht aller Termine')
    cy.get('[data-cy=collectives-card]').should('contain', 'Kollektive (eine Auswahl)')
    cy.get('[data-cy=collectives-card]').should('contain', 'Liste mit allen Kollektivbetrieben')
    // cy.get('[data-cy=offers-card]').should('contain', 'Marktplatz (eine Auswahl)')
    // cy.get('[data-cy=offers-card]').should('contain', 'Alle Angebote und Gesuche')
  })
})

