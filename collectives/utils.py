"""
collectives.utils - general helper functions for collectives
Copyright (C) 2020 >make Utopia_

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.core.mail import send_mail
from django.db.models import Q
from .models import Collective
from nominatim import Nominatim
import urllib.parse


def get_my_collectives(request):
    if request.user.is_authenticated is True:
        if "my_collectives" in request.session:
            my_collectives = request.session.get("my_collectives")
        else:
            collectives = Collective.objects.filter(
                Q(authors__id=request.user.id)
            )
            my_collectives = [{"id": c.id, "name": c.name} for c in collectives]
            request.session["my_collectives"] = my_collectives
    else:
        request.session.pop('my_collectives', None)
        my_collectives = []
    return my_collectives if my_collectives else None


def get_geodata_by_address(address, postal_code, city):
    if not address:
        if not postal_code:
            full_address = f"{city}"
        elif not city:
            return {}
        else:
            full_address = f"{postal_code} {city}"
    elif "&" in address:
        address = address.split("&")[0]
        full_address = f"{address}, {postal_code} {city}"
    else:
        full_address = f"{address}, {postal_code} {city}"
    nom = Nominatim()
    full_address = urllib.parse.quote(full_address)
    res = nom.query(full_address)
    if res:
        return res[0]
    return {}


def send_email(receiver, sender, subject, text):
        # The return value of send_mail will be the number of successfully
        # delivered messages (see: Django documentation).
        # 0 = zero send emails, 1 = one send email
        res = send_mail(
            subject,
            text,
            sender,
            [receiver],
            fail_silently=False,
        )
        return res
