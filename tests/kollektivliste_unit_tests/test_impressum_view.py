from django.test import Client, TestCase


class ImpressumViewTests(TestCase):

    def setUp(self):
        self.c = Client()

    def test_impressum_view(self):
        resp = self.c.get('/impressum/')
        self.assertEqual(resp.status_code, 200)
        content = resp.content.decode("utf-8")
        self.assertTrue("Datenschutzerklärung" in content)
        self.assertTrue("Impressum" in content)
