from accounts.models import User
from django.test import Client, TestCase


class LoginViewTests(TestCase):

    def setUp(self):
        volker = User(email="vl@it.kollektiv.hamburg")
        volker.set_password("keines0815")
        volker.save()
        self.c = Client()

    def test_login_view_get(self):
        resp = self.c.get('/accounts/login/')
        self.assertEqual(resp.status_code, 200)
        content = resp.content.decode("utf-8")
        self.assertTrue("username" in content)
        self.assertTrue("password" in content)

    def test_login_view_post_redirects_to_home_on_success(self):
        resp = self.c.post(
            "/accounts/login/",
            {"username": "vl@it.kollektiv.hamburg", "password": "keines0815"}
        )
        self.assertEqual(resp.status_code, 302)
        self.assertEqual(resp.url, "/")

    def test_login_view_post_fails_if_wrong_username(self):
        resp = self.c.post(
            "/accounts/login/",
            {"username": "vl@it.kollektiv.hamburg", "password": "0815keines"}
        )
        self.assertEqual(resp.status_code, 200)
        content = resp.content.decode("utf-8")
        self.assertTrue("Groß-/Kleinschreibung" in content)
