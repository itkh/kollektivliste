/*
collectives.custom-scripts.js - custom js scripts for collectives
Copyright (C) 2020 >make Utopia_

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
var CollectiveList = {
    init: function () {
        this.switchMenu();
    },
    switchMenu: function () {
        var w = window.innerWidth;
        if (w <= 992) {
            $('nav ul:first').removeAttr('class');
            $('nav ul:first').addClass('side-nav');
            $('nav ul:first').attr('id', 'mobile-nav');
        } else {
            $('nav ul:first').removeAttr('id');
            $('nav ul:first').removeAttr('class');
            $('nav ul:first').removeAttr('style');
            $('nav ul:first').addClass('right');
            $('nav ul:first').addClass('hide-on-med-and-down');
        }
    },
    adjustFilterboxOnScroll: function (evt) {
        var distance = $(".navbar-fixed").height();
        var filterPosition = $("#position-marker").offset().top - $(window).scrollTop();
        if ( filterPosition <= distance ) {
            $("#filterbox").addClass("sticky-filterbox");
            $("#filterbox").css("top", distance + "px");
        }
        if ( filterPosition > distance ) {
            $("#filterbox").removeClass("sticky-filterbox");
        }
    },
    pubUnPub: function () {
        // fix for styling the publish-selection as long as we have no access to the forms html structure
        var publishParagraph = $("#id_public").parent();
        $(publishParagraph).addClass("publish-box");
        $(publishParagraph).children().remove();
        // here we check if the collective is published and append the right checkbox
        if ( !$("form").hasClass("published") ) {
            var pubCheckbox = $('<input type="checkbox" name="public" id="id_public">');
            var labelText = 'Veröffentlichen? (Dein Beitrag ist dann für alle Besucher sichtbar)';
            var pubLabel = $('<label for="id_public">' + labelText + '</label>');
            $(pubCheckbox).appendTo(publishParagraph);
            $(pubLabel).appendTo(publishParagraph);
        } else if ( $("form").hasClass("published") ) {
            var unpubCheckbox = $('<input type="checkbox" name="public" checked="checked" id="id_public">');
            var labelText = 'Dein Beitrag ist derzeit öffentlich und für alle Besucher sichtbar';
            var unpubLabel = $('<label for="id_public">' + labelText + '</label>');
            $(unpubCheckbox).appendTo(publishParagraph);
            $(unpubLabel).appendTo(publishParagraph);
        }
    },
    submitFilterbox: function (selectId, formId) {
        var selectValue = document.getElementById(selectId).value;
        var otherForm = document.getElementById(formId);
        if (selectValue) {
            if (selectId.startsWith("region")) {
                otherForm.region.value = selectValue;
            } else if (selectId.startsWith("branch")) {
                otherForm.branche.value = selectValue;
            }
        }
        otherForm.submit();
    }
}

//remove empty paragraphs from the form
$('p').each(function() {
    var $this = $(this);
    if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
        $this.remove();
});

$( document ).ready(function() {
    var inst = Object.create(CollectiveList);
    inst.init()
    $(".button-collapse").sideNav();
    $(".dropdown-button").dropdown();
    $('select').material_select();
    var updateSite = document.getElementById("update-collective");
    if (updateSite) {
        var dataset = $('.autocomplete').data('business');
        $(".autocomplete").autocomplete({data: dataset});
        inst.pubUnPub();
    }
    var filterBox = document.getElementById("filterbox");
    if (filterBox) {
        window.addEventListener("scroll", inst.adjustFilterboxOnScroll);
        $("#branch-select").on('change', function() {
            inst.submitFilterbox("region-select", "branch-form");
        });
        $("#region-select").on('change', function() {
            inst.submitFilterbox("branch-select", "region-form");
        });
    }
    // http://archives.materializecss.com/0.100.2/forms.html#date-picker
    // https://amsul.ca/pickadate.js/date/
    $('.dateinput').pickadate({
        monthsFull: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
        monthsShort: ['Jan', 'Feb', 'Mrz', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
        weekdaysFull: ['Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag'],
        weekdaysShort: ['Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So'],
        labelMonthNext: 'Nächster Monat',
        labelMonthPrev: 'Vorheriger Monat',
        labelMonthSelect: 'Einen Monat auswählen',
        labelYearSelect: 'Ein Jahr auswählen',
        firstDay: 1,
        format: 'dd.mm.yyyy',
        formatSubmit: 'dd.mm.yyyy',
        selectMonths: true,
        selectYears: 4,
        min: true,
        max: false,
        today: 'Heute',
        clear: 'Löschen',
        close: 'Ok',
        closeOnSelect: false,
        container: undefined
    });
    // http://archives.materializecss.com/0.100.2/forms.html#time-picker
    $('.timeinput').pickatime({
        default: 'now',
        fromnow: 0,
        twelvehour: false,
        donetext: 'OK',
        cleartext: 'Löschen',
        canceltext: 'Abbrechen',
        container: undefined,
        autoclose: false,
        ampmclickable: true,
        aftershow: function(){}
    });
});

$( window ).resize(function() {
    var inst = Object.create(CollectiveList);
    inst.init()
	$(".button-collapse").sideNav();
});

//fix for wrong position of description-label when a checkbox appears(right below in order to remove a logo)
var descrLabel = $('label[for="id_description"]').parent();
$(descrLabel).addClass("full-width");

