from django.test import Client, TestCase


class AboutViewTests(TestCase):

    def setUp(self):
        self.c = Client()

    def test_about_view(self):
        resp = self.c.get('/')
        self.assertEqual(resp.status_code, 200)
        content = resp.content.decode("utf-8")
