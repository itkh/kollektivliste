from django.forms import ModelForm, BooleanField
from django.forms.models import inlineformset_factory
from .models import Collective, AdditionalAddress

class AddressForm(ModelForm):
    class Meta:
        model = AdditionalAddress
        fields = ['address', 'postal_code', 'city', 'country', 'current']

AddressFormSet = inlineformset_factory(
    Collective, 
    AdditionalAddress,
    form=AddressForm,
    extra=1,
    can_delete=True,
)
