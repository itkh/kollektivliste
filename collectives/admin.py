"""
collectives.admin - Register collectives to django admin sites
Copyright (C) 2020 >make Utopia_

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty oftrash:/Good Bye Lenin - ZDFmediathek.webm
trash:/Vertrauter Feind - ZDFmediathek.webm
trash:/Fall - ZDFmediathek.webm

MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.contrib import admin
from .models import Collective, KdKMember, AdditionalAddress, Checked, BusinessClass, CheckResultClass

class AdditionalAddressInline(admin.StackedInline):
    model = AdditionalAddress
    min_num = 0
    extra = 0
    fields = [
        'address', 
        ('postal_code', 'city'), 
        'country',
        ('lat', 'lon'), 
        'current'
    ]


class KdKMemberInline(admin.StackedInline):
    model = KdKMember
    min_num = 0
    extra = 0
    fields = [
        'since', 'till'
    ]


class CheckedInline(admin.StackedInline):
    model = Checked
    min_num = 0
    extra = 0
    fields = [
        ('checker', 'checktime'),
        'result'
    ]
    readonly_fields = ('checktime', ) 


@admin.register(Collective)
class CollectiveAdmin(admin.ModelAdmin):
    title = "Kollektive"

    list_display = ('name', )
    
    inlines = [
        AdditionalAddressInline,
        KdKMemberInline,
        CheckedInline,
    ]


@admin.register(BusinessClass)
class BusinessClassAdmin(admin.ModelAdmin):
    titel = "Branchen"
    
    list_display = ("getFullString", )

admin.site.register(CheckResultClass)


