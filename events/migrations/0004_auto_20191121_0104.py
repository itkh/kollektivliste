# Generated by Django 2.2.7 on 2019-11-21 00:04

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('events', '0003_auto_20191108_2145'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='author',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='event',
            name='provider',
            field=models.CharField(blank=True, default='', max_length=100, verbose_name='Veranstalter (nur eintragen, wenn verschieden vom Kollektiv)'),
        ),
        migrations.AlterField(
            model_name='event',
            name='address',
            field=models.CharField(blank=True, max_length=256, verbose_name='Straße und Hausnummer (nur eintragen, wenn verschieden vom Kollektiv)'),
        ),
        migrations.AlterField(
            model_name='event',
            name='city',
            field=models.CharField(blank=True, max_length=50, verbose_name='Stadt (nur eintragen, wenn verschieden vom Kollektiv)'),
        ),
        migrations.AlterField(
            model_name='event',
            name='collective',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='collectives.Collective', verbose_name='Kollektiv'),
        ),
        migrations.AlterField(
            model_name='event',
            name='email',
            field=models.EmailField(blank=True, default='', max_length=254, verbose_name='E-Mail-Adresse (nur eintragen, wenn verschieden vom Kollektiv)'),
        ),
        migrations.AlterField(
            model_name='event',
            name='postal_code',
            field=models.CharField(blank=True, max_length=5, verbose_name='Postleitzahl (nur eintragen, wenn verschieden vom Kollektiv)'),
        ),
        migrations.AlterField(
            model_name='event',
            name='website',
            field=models.URLField(blank=True, default='', max_length=100, verbose_name='Webseite (nur eintragen, wenn verschieden vom Kollektiv)'),
        ),
    ]
