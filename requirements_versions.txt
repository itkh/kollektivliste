asgiref==3.3.4
Django==3.2.4
django-ckeditor==6.1.0
django-crispy-forms==1.11.2
django-js-asset==1.2.2
nominatim==0.1
Pillow==8.2.0
psycopg2==2.8.6
python-dateutil==2.8.1
pytz==2021.1
six==1.15.0
sqlparse==0.4.1
typing-extensions==3.10.0.0

