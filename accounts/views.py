"""
accounts.views - views for accounts
Copyright (C) 2020 >make Utopia_

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.urls import reverse_lazy
from django.views import generic
from .forms import CustomUserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin


class CreateUserView(LoginRequiredMixin, UserPassesTestMixin,
                     generic.CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('startseite')
    template_name = 'add_user.html'

    def test_func(self):
        return self.request.user.is_staff

    def form_valid(self, form):
        return super().form_valid(form)
