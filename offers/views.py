"""
offers.views - views for offers
Copyright (C) 2020 >make Utopia_

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from CollectiveList.forms import ContactForm
from collectives.models import Collective
from collectives.utils import get_my_collectives, send_email
import datetime
from django.apps import apps as django_apps
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormMixin
from .models import Offer


def get_collectives_as_choices(user):
    default = [("", "Ein Kollektiv auswählen")]
    if user.is_superuser is True:
        collectives = Collective.objects.filter(public=True)
    else:
        collectives = Collective.objects.filter(
            Q(authors__id=user.id) & Q(public=True)
        )
    my_collectives = [(c.id, c.name) for c in collectives]
    choices = [*default, *my_collectives]
    return choices


class OfferListView(ListView):
    model = Offer
    template_name = 'offer_list.html'

    def setup(self, request, *args, **kwargs):
        setattr(request, "app_config", django_apps.get_app_config("offers"))
        return super().setup(request, *args, **kwargs)

    def get_queryset(self):
        limit = datetime.date.today() - datetime.timedelta(days=42)
        queryset = Offer.objects.filter(modified_time__date__gt=limit)
        queryset = queryset.order_by('modified_time')
        return queryset

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['my_collectives'] = get_my_collectives(self.request)
        data["site_title"] = "Angebote und Gesuche von Kollektivbetrieben"
        data["site_keywords"] = "Marktplatz, Kollektivbetriebe, Kollektive"
        data["site_description"] = "Marktplatz mit Angeboten und Gesuchen von \
Kollektivbetrieben"
        return data


class OfferDetailView(FormMixin, DetailView):
    model = Offer
    template_name = 'offer_detail.html'
    form_class = ContactForm

    def setup(self, request, *args, **kwargs):
        setattr(request, "app_config", django_apps.get_app_config("offers"))
        return super().setup(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['my_collectives'] = get_my_collectives(self.request)
        for a in ["email", "website", "address", "postal_code", "city", "lat", "lon"]:
            if not getattr(data["object"], a) and hasattr(data["object"].collective, a):
                setattr(data['object'], a, getattr(data["object"].collective, a))
        data["object"].lat = str(data["object"].lat) if data["object"].lat else ""
        data["object"].lon = str(data["object"].lon) if data["object"].lon else ""
        subject = "{}: {}".format(
            data["object"].sort, data["object"].title)
        self.initial = {
            "receiver": data["object"].email,
            "subject": subject
        }
        data['form'] = self.get_form()
        result = self.request.path[self.request.path.rfind("/") + 1:]
        data['result'] = result
        data["site_title"] = "Angebote und Gesuche von Kollektivbetrieben"
        return data

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(request, form)
        else:
            return self.form_invalid(request, form)

    def form_valid(self, request, form):
        send_email(
            request.POST.get("receiver"),
            request.POST.get("sender"),
            request.POST.get("subject"),
            request.POST.get("text")
        )
        self.success_url = reverse_lazy('offer_detail_result', kwargs={"pk": self.object.id, "result": "success"})
        return super().form_valid(form)


class OfferCreateView(LoginRequiredMixin, CreateView):
    model = Offer
    template_name = 'offer_new.html'
    fields = ('sort', 'title', 'description', 'collective','provider',
              'website', 'email', 'address', 'postal_code', 'city')

    def setup(self, request, *args, **kwargs):
        setattr(request, "app_config", django_apps.get_app_config("offers"))
        return super().setup(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        my_choices = get_collectives_as_choices(self.request.user)
        data = super().get_context_data(**kwargs)
        data['ddval'] = 'Anzeige eintragen'
        data['form'].fields.get('sort').widget.attrs['class'] = 'validate'
        data['form'].fields.get('sort').widget.attrs['oninvalid'] = "this.setCustomValidity('Bitte wählte eine Art des Eintrags aus!')"
        data['form'].fields.get('title').widget.attrs['class'] = 'validate'
        data['form'].fields.get('title').widget.attrs['oninvalid'] = "this.setCustomValidity('Bitte gib einen Titel für den Eintrag an!')"
        data['form'].fields.get("collective").choices = my_choices
        data['my_collectives'] = get_my_collectives(self.request)
        return data


class OfferUpdateView(LoginRequiredMixin, UpdateView):
    model = Offer
    template_name = 'offer_update.html'
    fields = ('sort', 'title', 'description', 'collective', 'provider',
              'website', 'email', 'address', 'postal_code', 'city')

    def setup(self, request, *args, **kwargs):
        setattr(request, "app_config", django_apps.get_app_config("offers"))
        return super().setup(request, *args, **kwargs)

    def form_valid(self, form):
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['form'].fields.get('sort').widget.attrs['class'] = 'validate'
        data['form'].fields.get('sort').widget.attrs['oninvalid'] = "this.setCustomValidity('Bitte wählte eine Art des Eintrags aus!')"
        data['form'].fields.get('title').widget.attrs['class'] = 'validate'
        data['form'].fields.get('title').widget.attrs['oninvalid'] = "this.setCustomValidity('Bitte gib einen Titel für den Eintrag an!')"
        data['my_collectives'] = get_my_collectives(self.request)
        return data


class OfferDeleteView(LoginRequiredMixin, DeleteView):
    model = Offer
    template_name = 'offer_delete.html'
    success_url = reverse_lazy('offer_list')

    def setup(self, request, *args, **kwargs):
        setattr(request, "app_config", django_apps.get_app_config("offers"))
        return super().setup(request, *args, **kwargs)
