"""
events.models - Database model for Event
Copyright (C) 2020 >make Utopia_

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.db import models
from django.urls import reverse
from tinymce.models import HTMLField 


class Event(models.Model):
    title = models.CharField(
        max_length=200,
        default="",
        verbose_name="Titel")
    description = HTMLField(
        blank=True,
        default="",
        verbose_name="Beschreibung")
    start_date = models.DateField(
        verbose_name="Datum")
    start_time = models.TimeField(
        blank=True,
        null=True,
        verbose_name="Beginn")
    center_name = models.CharField(
        max_length=200,
        default="",
        verbose_name="Name des Veranstaltungsortes")
    social_media_site = models.URLField(
        blank=True,
        default="",
        max_length=100,
        verbose_name="Social Media Seite der Veranstaltung")
    collective = models.ForeignKey(
        'collectives.Collective',
        related_name="events",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name="Kollektiv")
    provider = models.CharField(
        blank=True,
        default="",
        max_length=100,
        verbose_name="Veranstalter (nur eintragen, wenn verschieden vom Kollektiv)")
    website = models.URLField(
        blank=True,
        default="",
        max_length=100,
        verbose_name="Webseite (nur eintragen, wenn verschieden vom Kollektiv)")
    email = models.EmailField(
        blank=True,
        default="",
        verbose_name="E-Mail-Adresse (nur eintragen, wenn verschieden vom Kollektiv)")
    address = models.CharField(
        blank=True,
        max_length=256,
        verbose_name="Straße und Hausnummer (nur eintragen, wenn verschieden vom Kollektiv)")
    postal_code = models.CharField(
        blank=True,
        max_length=5,
        verbose_name="Postleitzahl (nur eintragen, wenn verschieden vom Kollektiv)")
    city = models.CharField(
        blank=True,
        max_length=50,
        verbose_name="Stadt (nur eintragen, wenn verschieden vom Kollektiv)")
    country = models.CharField(
        blank=True,
        max_length=100,
        default="Deutschland",
        verbose_name="Land")
    lat = models.DecimalField(
        blank=True,
        max_digits=9,
        decimal_places=6,
        null=True)
    lon = models.DecimalField(
        blank=True,
        max_digits=9,
        decimal_places=6,
        null=True)
    author = models.ForeignKey(
        'accounts.User',
        null=True,
        on_delete=models.SET_NULL)
    modified_time = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ["start_date"]
        verbose_name = "Veranstaltung"
        verbose_name_plural = "Veranstaltungen"

    def __str__(self):
        """A string representation of the model."""
        return self.title[:100]

    def get_absolute_url(self):
        return reverse('event_detail', args=[str(self.id)])
