"""
CollectiveList.views - views for CollectiveList site
Copyright (C) 2020 >make Utopia_

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from collectives.utils import get_my_collectives, send_email
import datetime
from django.apps import apps as django_apps
from django.conf import settings
from django.db.models import Func
from django.http import HttpResponseBadRequest
from django.shortcuts import render
from django.views.generic import View, ListView, TemplateView
from .forms import ContactForm, NewsletterForm
from collectives.models import Collective
from events.models import Event
from offers.models import Offer
import random


collate = Func(
    'name',
    function='de_DE',
    template='(%(expressions)s) COLLATE "%(function)s"')


def choose_ids(ids, sample):
    if len(ids) <= sample:
        return ids
    choosed_ids = random.sample(ids, sample)
    return choosed_ids


class AboutPageView(TemplateView):
    template_name = 'about.html'

    def setup(self, request, *args, **kwargs):
        setattr(request, "app_config", django_apps.get_app_config("collectives"))
        return super().setup(request, *args, **kwargs)


class ImpressumPageView(TemplateView):
    template_name = 'impressum.html'

    def setup(self, request, *args, **kwargs):
        setattr(request, "app_config", django_apps.get_app_config("collectives"))
        return super().setup(request, *args, **kwargs)


class LinksPageView(TemplateView):
    template_name = 'links.html'

    def setup(self, request, *args, **kwargs):
        setattr(request, "app_config", django_apps.get_app_config("collectives"))
        return super().setup(request, *args, **kwargs)


class ContactPageView(View):
    template_name = 'contact.html'

    def setup(self, request, *args, **kwargs):
        setattr(request, "app_config", django_apps.get_app_config("collectives"))
        return super().setup(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        success = None
        name = "uns"
        receiver = (request.GET.get("rec") if request.GET.get("rec")
                    else settings.CONTACT_EMAIL_RECEIVER)
        if receiver != settings.CONTACT_EMAIL_RECEIVER:
            try:
                collective = Collective.objects.filter(email=receiver).get()
            except Collective.DoesNotExist:
                text = "No collective found for your input: {}.".format(
                    receiver)
                return HttpResponseBadRequest(text)
            else:
                name = collective.name
        form = ContactForm(initial={"receiver": receiver})
        my_collectives = get_my_collectives(request)
        return render(request,
                      self.template_name,
                      {'form': form, 'my_collectives': my_collectives,
                       'success': success, 'name': name})

    def post(self, request, *args, **kwargs):
        success = None
        name = "uns"
        form = ContactForm(request.POST)
        my_collectives = get_my_collectives(request)
        if form.is_valid():
            receiver = request.POST.get("receiver", settings.CONTACT_EMAIL_RECEIVER)
            if receiver != settings.CONTACT_EMAIL_RECEIVER:
                try:
                    collective = Collective.objects.filter(email=receiver).get()
                except Collective.DoesNotExist:
                    text = "No collective found for your input: {}.".format(
                        receiver)
                    return HttpResponseBadRequest(text)
                else:
                    name = collective.name
            res = send_email(
                receiver,
                request.POST.get("sender"),
                request.POST.get("subject"),
                request.POST.get("text")
            )
            if res == 0:
                success = False
            else:
                form = ContactForm()
                success = True
        else:
            success = False
        return render(request,
                      self.template_name,
                      {'form': form, 'my_collectives': my_collectives,
                       'success': success, 'name': name})


class HomePageView(ListView):
    template_name = 'home_page.html'

    def setup(self, request, *args, **kwargs):
        setattr(request, "app_config", django_apps.get_app_config("collectives"))
        return super().setup(request, *args, **kwargs)

    def get_queryset(self):
        queryset = Collective.objects.filter(public=True)
        all_ids = [c.id for c in queryset]
        selected_ids = choose_ids(all_ids, 10)
        queryset = queryset.filter(pk__in=selected_ids)
        queryset = queryset.order_by(collate.asc())
        queryset = queryset.distinct()
        return queryset

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        today = datetime.date.today()
        events = Event.objects.filter(start_date__gte=today)
        events = events.order_by('start_date')

        limit = today - datetime.timedelta(days=42)
        offers = Offer.objects.filter(modified_time__date__gt=limit)
        offers = offers.order_by('modified_time')

        data['my_collectives'] = get_my_collectives(self.request)
        data['events'] = events[:2] if len(events) > 3 else events
        data['offers'] = offers[:2] if len(offers) > 3 else offers
        data["site_title"] = "Alles rund um Kollektivbetriebe"
        data["site_keywords"] = "Kollektivbetriebe, Kollektive, Termine, Kalender, Deutschland"
        data["site_description"] = "Informationen, Veranstaltungen und Angebote von \
Kollektivbetrieben aus dem deutschsprachigen Raum"
        return data


class NewsletterPageView(View):
    template_name = 'newsletter.html'

    def setup(self, request, *args, **kwargs):
        setattr(request, "app_config", django_apps.get_app_config("collectives"))
        return super().setup(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        success = None
        form = NewsletterForm()
        my_collectives = get_my_collectives(request)
        return render(request,
                      self.template_name,
                      {'form': form, 'my_collectives': my_collectives,
                       'success': success})

    def post(self, request, *args, **kwargs):
        success = None
        form = NewsletterForm(request.POST)
        my_collectives = get_my_collectives(request)
        if form.is_valid():
            res = send_email(
                settings.NEWSLETTER_SUBSCRIBE_RECEIVER,
                request.POST.get("sender"),
                settings.NEWSLETTER_SUBSCRIBE_SUBJECT,
                settings.NEWSLETTER_SUBSCRIBE_TEXT
            )
            if res == 0:
                success = False
            else:
                form = NewsletterForm()
                success = True
        else:
            success = False
        return render(request,
                      self.template_name,
                      {'form': form, 'my_collectives': my_collectives,
                       'success': success})
