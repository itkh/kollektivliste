"""
events.views - views for events
Copyright (C) 2020 >make Utopia_

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from collectives.models import Collective
from collectives.utils import get_my_collectives
import datetime
from django.apps import apps as django_apps
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.urls import reverse_lazy
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Event


def get_collectives_as_choices(user):
    default = [("", "Ein Kollektiv auswählen")]
    if user.is_superuser is True:
        collectives = Collective.objects.filter(public=True)
    else:
        collectives = Collective.objects.filter(
            Q(authors__id=user.id) & Q(public=True)
        )
    my_collectives = [(c.id, c.name) for c in collectives]
    choices = [*default, *my_collectives]
    return choices


class EventListView(ListView):
    template_name = 'event_list.html'

    def setup(self, request, *args, **kwargs):
        setattr(request, "app_config", django_apps.get_app_config("events"))
        return super().setup(request, *args, **kwargs)

    def get_queryset(self):
        today = datetime.date.today()
        queryset = Event.objects.filter(start_date__gte=today)
        queryset = queryset.order_by('start_date')
        return queryset

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['my_collectives'] = get_my_collectives(self.request)
        data["site_title"] = "Termine von Kollektivbetrieben aus dem \
deutschsprachigen Raum"
        data["site_keywords"] = "Kollektivbetriebe, Veranstaltungskalender, \
Kollektive, Terminkalender, Deutschland"
        data["site_description"] = "Veranstaltungskalender von \
Kollektivbetrieben aus dem deutschsprachigen Raum"
        return data


class EventCreateView(LoginRequiredMixin, CreateView):
    model = Event
    template_name = 'event_new.html'
    fields = ('title', 'description', 'center_name', 'start_date',
              'start_time', 'social_media_site', 'collective', 'provider',
              'website', 'email', 'address', 'postal_code', 'city')
    success_url = reverse_lazy('event_list')

    def setup(self, request, *args, **kwargs):
        setattr(request, "app_config", django_apps.get_app_config("events"))
        return super().setup(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        my_choices = get_collectives_as_choices(self.request.user)
        data = super().get_context_data(**kwargs)
        data['ddval'] = 'Termin eintragen'
        data['form'].fields.get('title').widget.attrs['class'] = 'validate'
        data['form'].fields.get('title').widget.attrs['oninvalid'] = "this.setCustomValidity('Bitte gib einen Titel für den Termin an!')"
        data['form'].fields.get('center_name').widget.attrs['class'] = 'validate'
        data['form'].fields.get('center_name').widget.attrs['oninvalid'] = "this.setCustomValidity('Bitte gib einen Veranstaltungsort für den Termin an!')"
        data['form'].fields.get("collective").choices = my_choices
        data['form'].fields.get("start_date").widget.input_type = 'date'
        data['form'].fields.get("start_time").widget.input_type = 'time'
        data['my_collectives'] = get_my_collectives(self.request)
        return data


class EventUpdateView(LoginRequiredMixin, UpdateView):
    model = Event
    template_name = 'event_update.html'
    fields = ('title', 'description', 'center_name', 'start_date',
              'start_time', 'social_media_site', 'collective', 'provider',
              'website', 'email', 'address', 'postal_code', 'city')
    success_url = reverse_lazy('event_list')

    def setup(self, request, *args, **kwargs):
        setattr(request, "app_config", django_apps.get_app_config("events"))
        return super().setup(request, *args, **kwargs)

    def form_valid(self, form):
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['my_collectives'] = get_my_collectives(self.request)
        data['form'].fields.get('title').widget.attrs['class'] = 'validate'
        data['form'].fields.get('title').widget.attrs['oninvalid'] = "this.setCustomValidity('Bitte gib einen Titel für den Termin an!')"
        data['form'].fields.get('center_name').widget.attrs['class'] = 'validate'
        data['form'].fields.get('center_name').widget.attrs['oninvalid'] = "this.setCustomValidity('Bitte gib einen Veranstaltungsort für den Termin an!')"
        return data


class EventDeleteView(LoginRequiredMixin, DeleteView):
    model = Event
    template_name = 'event_delete.html'
    success_url = reverse_lazy('event_list')

    def setup(self, request, *args, **kwargs):
        setattr(request, "app_config", django_apps.get_app_config("events"))
        return super().setup(request, *args, **kwargs)
