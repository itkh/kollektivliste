# Generated by Django 2.2.7 on 2019-11-21 00:04

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('offers', '0010_auto_20191108_2157'),
    ]

    operations = [
        migrations.AddField(
            model_name='offer',
            name='author',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='offer',
            name='provider',
            field=models.CharField(blank=True, default='', max_length=100, verbose_name='Anbieter (nur eintragen, wenn verschieden vom Kollektiv)'),
        ),
        migrations.AlterField(
            model_name='offer',
            name='collective',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='collectives.Collective', verbose_name='Kollektiv'),
        ),
    ]
