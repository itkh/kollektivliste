"""
events.urls - url registration for events
Copyright (C) 2020 >make Utopia_

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.urls import path
from . import views

urlpatterns = [
    path('event/list/', views.EventListView.as_view(), name='event_list'),
    path('event/new/', views.EventCreateView.as_view(), name='event_new'),
    path('event/<int:pk>/update/', views.EventUpdateView.as_view(),
         name='event_update'),
    path('event/<int:pk>/delete/', views.EventDeleteView.as_view(),
         name='event_delete'),
]
