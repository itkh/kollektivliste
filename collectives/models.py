"""
collectives.models - Database model for Collective
Copyright (C) 2020 >make Utopia_

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.db import models
from django.urls import reverse
from tinymce.models import HTMLField


class BusinessClass(models.Model):
    parent = models.ForeignKey('self', on_delete=models.SET_NULL, blank=True, null=True, related_name='children')
    name = models.CharField(
        max_length=50,
        verbose_name="Branche",
        unique=True)

    class Meta:
        verbose_name = 'Branche'
        verbose_name_plural = 'Branchen'

    def getAllParents(self):
        parent = self.parent
        parentList = []
        while parent is not None:
            parentList.append(parent)
            parent = parent.parent
        return parentList
    
    def gellAllChildren(self, child = None):
        if child is None:
            children = self.children.all()
        else:
            children = child.children.all()
        childList = list(children)
        for child in children:
            childList += self.gellAllChildren(child)
        return childList

    def __str__(self):
        """A string representation of the model."""
        return f"{'- ' * len(self.getAllParents())}{self.name}"

    def getFullString(self):
        ret = ""
        for p in reversed(self.getAllParents()):
            ret += f"{p.name}/"
        ret += f"{self.name}"
        return ret


class Collective(models.Model):
    name = models.CharField(
        max_length=100,
        verbose_name="Name des Kollektivs")
    title = models.CharField(
        max_length=200,
        blank=True,
        verbose_name="Beschreibung (Stichworte)")
    logo = models.ImageField(
        "euer logo",
        blank=True,
        null=True,
        upload_to='logos')
    bgpic = models.ImageField(
        "Euer Hintergrundbild (Es sollte im Querformat und nicht mehr als 2MB groß sein.)",
        blank=True,
        null=True,
        upload_to='bgpics')
    description = HTMLField(
        blank=True,
        verbose_name="Ausführliche Beschreibung")
    website = models.URLField(
        blank=True,
        max_length=100,
        verbose_name="Webseite")
    email = models.EmailField(
        blank=True,
        max_length=100,
        verbose_name="Email")
    businessClassList = models.ManyToManyField(BusinessClass, verbose_name="Branchen")
    last_editor = models.ForeignKey(
        'accounts.User',
        null=True,
        on_delete=models.SET_NULL)
    last_edit_time = models.DateTimeField(
        auto_now=True,
        null=True)
    public = models.BooleanField(
        default=False,
        verbose_name='veröffentlichen')
    authors = models.ManyToManyField(
        'accounts.User',
        related_name="AuthorInnen",
        verbose_name='Author*innen')

    class Meta:
        ordering = ["name"]
        verbose_name = "Kollektiv"
        verbose_name_plural = "Kollektive"

    def __str__(self):
        """A string representation of the model."""
        return self.name[:50]

    def get_absolute_url(self):
        return reverse('collective_detail', args=[str(self.id)])


class KdKMember(models.Model):
    collective = models.ForeignKey(Collective, on_delete=models.CASCADE)
    since = models.DateField(verbose_name="Mitglied seit")
    till = models.DateField(verbose_name="Mitglied bis", blank=True, null=True)
    
    class Meta:
        verbose_name = 'KdK Mitglied'
        verbose_name_plural = 'KdK Mitglieder'

    def __str__(self):
        """A string representation of the model."""
        bis = ""
        if self.till is not None:
            bis = f" bis {self.till:%d.%m.%Y}"
        return f"{self.collective.name[:50]} seit {self.since:%d.%m.%Y}{bis}"

        
class AdditionalAddress(models.Model):
    collective = models.ForeignKey(Collective, on_delete=models.CASCADE, related_name='addresses')
    address = models.CharField(
        blank=True,
        max_length=256,
        verbose_name="Straße und Hausnummer")
    postal_code = models.CharField(
        blank=True,
        max_length=5,
        verbose_name="PLZ")
    city = models.CharField(
        blank=False,
        max_length=50,
        verbose_name="Stadt")
    country = models.CharField(
        blank=True,
        max_length=100,
        default="Deutschland",
        verbose_name="Land")
    lat = models.DecimalField(
        blank=True,
        max_digits=9,
        decimal_places=6,
        null=True)
    lon = models.DecimalField(
        blank=True,
        max_digits=9,
        decimal_places=6,
        null=True)
    current = models.BooleanField(
        default=False,
        verbose_name='Aktuelle Adresse')

    class Meta:
        verbose_name = 'Adresse'
        verbose_name_plural = 'Adressen'

    def __str__(self):
        """A string representation of the model."""
        return f"{self.collective.name[:50]}\n{self.address}\n{self.postal_code} {self.city}"

class CheckResultClass(models.Model):
    name = models.CharField(
        primary_key=True,
        max_length=20)

    class Meta:
        verbose_name = 'Prüfungs Ergebnis'
        verbose_name_plural = 'Prüfungs Ergebnisse'

    def __str__(self):
        """A string representation of the model."""
        return f"{self.name}"
        
class Checked(models.Model):
    collective = models.ForeignKey(Collective, on_delete=models.CASCADE)
    checker = models.ForeignKey(
        'accounts.User',
        null=True,
        on_delete=models.SET_NULL)
    checktime = models.DateTimeField(
        auto_now_add=True,
        null=True)
    result = models.ForeignKey(CheckResultClass, on_delete=models.CASCADE, verbose_name='Ergebnis')

    class Meta:
        verbose_name = 'Überprüft'
        verbose_name_plural = 'Überprüfungen'

    def __str__(self):
        """A string representation of the model."""
        return f"{self.collective.name[:50]} {self.checktime:%d.%m.%Y}:{self.result}"

    