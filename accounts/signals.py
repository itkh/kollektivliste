"""
accounts.signals - signal handler for accounts
Copyright (C) 2020 >make Utopia_

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from accounts.models import User
from collectives.utils import send_email
from django.conf import settings
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver


@receiver(post_save, sender=User, dispatch_uid="subscribe_new_user_to_mailinglist")
def subscribe_new_user_to_mailinglist(sender, instance, **kwargs):
    # if user is created, we send a subscribe request email to the
    # internal kollektivliste mailinglist.
    created = kwargs.get("created", False)
    if created is False:
        return
    send_email(
        settings.MAILINGLIST_SUBSCRIBE_RECEIVER,
        instance.email,
        settings.MAILINGLIST_SUBSCRIBE_SUBJECT,
        settings.MAILINGLIST_SUBSCRIBE_TEXT)


@receiver(post_delete, sender=User, dispatch_uid="unsubscribe_user_from_mailinglist")
def unsubscribe_user_from_mailinglist(sender, instance, **kwargs):
    # if user is deleted, then we send an unsubscribe request email to the
    # internal kollektivliste mailinglist.
    send_email(
        settings.MAILINGLIST_UNSUBSCRIBE_RECEIVER,
        instance.email,
        settings.MAILINGLIST_UNSUBSCRIBE_SUBJECT,
        settings.MAILINGLIST_UNSUBSCRIBE_TEXT)
