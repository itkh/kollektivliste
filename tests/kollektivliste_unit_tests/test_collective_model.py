from django.test import TestCase
from collectives.models import Collective


class CollectiveTestCase(TestCase):

    def setUp(self):
        Collective.objects.create(name="make_utopia")

    def test_collective_is_object_with_properties(self):
        it_collective = Collective.objects.get(name="make_utopia")
        self.assertTrue(isinstance(it_collective, Collective))
        self.assertEqual(str(it_collective), "make_utopia")
        self.assertFalse(it_collective.public)

    def test_collective_method_get_absolute_url(self):
        it_collective = Collective.objects.get(name="make_utopia")
        collective_url = it_collective.get_absolute_url()
        self.assertEqual(
            collective_url, "/collective/{}/".format(it_collective.pk))
