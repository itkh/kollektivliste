"""
collectives.views - views for collectives
Copyright (C) 2020 >make Utopia_

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from collectives.utils import get_my_collectives
from django.apps import apps as django_apps
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import Permission
from django.core.exceptions import PermissionDenied
from django.db.models import Func, Q, Count
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.shortcuts import get_object_or_404
import json
from .models import Collective, CheckResultClass, Checked, AdditionalAddress, BusinessClass, KdKMember
from .forms import AddressFormSet
import time
from .utils import get_geodata_by_address
from .plz import postal_codes
from datetime import date

#collate = Func(
#    'name',
#    function='de_DE',
#    template='(%(expressions)s) COLLATE "%(function)s"')

collate = Func(
    'name',
    function='LOWER')


class CollectiveListView(ListView):
    template_name = 'collective_list.html'

    def setup(self, request, *args, **kwargs):
        setattr(request, "app_config", django_apps.get_app_config("collectives"))
        return super().setup(request, *args, **kwargs)

    def get_queryset(self):
        if self.request.user.is_superuser is True:
            queryset = Collective.objects.all()
        elif self.request.user.is_authenticated is False:
            queryset = Collective.objects.filter(public=True)
        else:
            queryset = Collective.objects.filter(
                Q(authors__id=self.request.user.id) | Q(public=True)
            )
        if self.request.GET.get('branche', None):
            branch = BusinessClass.objects.get(pk=self.request.GET['branche'])
            if branch is not None:
                branchList = branch.gellAllChildren()
                branchList.append(branch)
                queryset = queryset.filter(businessClassList__in=branchList)
        if self.request.GET.get('region', None):
            if self.request.GET.get('region') == "keine":
                queryset = queryset.annotate(
                    numaddresses=Count('addresses')
                ).filter(numaddresses=0)
            else:
                postal_codes_in_state = self.get_postal_codes_for_state(self.request.GET['region'])
                addresses = AdditionalAddress.objects.filter(postal_code__in=postal_codes_in_state)
                queryset = queryset.filter(
                    Q(addresses__in=addresses)
                )
        queryset = queryset.order_by(collate.asc())
        queryset = queryset.distinct()
        return queryset

    def get_postal_codes_for_state(self, state):
        postal_codes_in_state = []
        for k in postal_codes:
            if postal_codes[k]["state"] == state:
                postal_codes_in_state.append(k)
        return postal_codes_in_state

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        # we can not use data['object_list'] because of queryset filtered
        # by branche. branches should have the branch of all collectives for
        # this user
        if self.request.user.is_superuser is True:
            collectives = Collective.objects.all()
        elif self.request.user.is_authenticated is False:
            collectives = Collective.objects.filter(public=True)
        else:
            collectives = Collective.objects.filter(
                Q(authors__id=self.request.user.id) | Q(public=True)
            )
        branches = BusinessClass.objects.all()   
        data["branches"] = branches
        data["selected_business"] = self.request.GET.get("branche", None)
        if data["selected_business"] is not None and data["selected_business"] != "":
            data["selected_business"] = int(data["selected_business"])
        regions = [postal_codes[a.postal_code]["state"] for c in collectives for a in c.addresses.all() if a.postal_code]
        data['regions'] = sorted(list(set(regions)))
        data['selected_region'] = self.request.GET.get('region', None)
        data['my_collectives'] = get_my_collectives(self.request)
        data["site_title"] = "Kollektivbetriebe aus dem deutschsprachigen Raum"
        data["site_keywords"] = "Kollektivbetriebe, Kollektive, Deutschland"
        data["site_description"] = "Liste mit Kollektivbetrieben aus dem \
deutschsprachigen Raum"
        return data


class CollectiveDetailView(DetailView):
    model = Collective
    template_name = 'collective_detail.html'

    def setup(self, request, *args, **kwargs):
        setattr(request, "app_config", django_apps.get_app_config("collectives"))
        return super().setup(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['my_collectives'] = get_my_collectives(self.request)
        if data.get("collective").public is False:
            user = self.request.user
            authors = data.get("collective").authors.values()
            author_ids = [int(a.get("id")) for a in authors]
            if user.is_superuser is False and user.id not in author_ids:
                raise PermissionDenied
        data["site_title"] = data.get("collective").name
        data["site_keywords"] = f"{data.get('collective').name}"
        data["business"] = ""
        for bc in data.get('collective').businessClassList.all():
            data["site_keywords"] += f", {bc.name}"
            if data["business"] != "":
                data["business"] += ", "
            data["business"] += bc.name
        for address in data.get("collective").addresses.all():
            data["site_keywords"] += f", {address.city}, {address.country}"
        data["site_description"] = data.get("collective").title
        data["checked"] = Checked.objects.filter(collective=data.get("collective")).order_by('-checktime').first()
        data["kdkMember"] = KdKMember.objects.filter(collective=data.get("collective")).order_by('-since').first()
        data["adresses"] = AdditionalAddress.objects.filter(collective=data.get("collective"), current=True)
        return data


class CollectiveCreateView(LoginRequiredMixin, UserPassesTestMixin,
                           CreateView):
    model = Collective
    template_name = 'collective_new.html'
    fields = ('name', 'authors')

    def setup(self, request, *args, **kwargs):
        setattr(request, "app_config", django_apps.get_app_config("collectives"))
        return super().setup(request, *args, **kwargs)

    def test_func(self):
        return self.request.user.is_staff

    def form_valid(self, form):
        form.instance.last_editor = self.request.user
        form.instance.last_edit_time = time.time()
        del self.request.session["my_collectives"]
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['ddval'] = 'Kollektiv eintragen'
        data['form'].fields['authors'].choices = sorted(
            data['form'].fields['authors'].choices,
            key=lambda x: x[1])
        data['group_permissions'] = Permission.objects.filter(
            group__user=self.request.user)
        data['my_collectives'] = get_my_collectives(self.request)
        return data


class CollectiveUpdateView(LoginRequiredMixin, UpdateView):
    model = Collective
    template_name = 'collective_update.html'
    fields = ('name', 'title', 'logo', 'bgpic', 'description', 'website',
              'email', 'public')
    
    def setup(self, request, *args, **kwargs):
        setattr(request, "app_config", django_apps.get_app_config("collectives"))
        return super().setup(request, *args, **kwargs)

    def form_valid(self, form):
        # if value of public checkbox was not changed, public is not in data
        # if public is not in data, but is set to true in db, the form
        # will set public field to changed and set db instance to false.
        # this hack solves the problem: if public is not in data, but is
        # set as changed_data, then we set the form instance to the initial
        # db value.
        if form.data.get('public', None) is None and 'public' in form.changed_data:
            form.instance.public = form.initial['public']
        form.instance.last_editor = self.request.user
        context = self.get_context_data()
        addresses = context["addresses_form"]
        if addresses.is_valid():
            addresses.instance = self.object
            addresses.save()
        else:
            print("invalid", addresses)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        user = self.request.user
        authors = data.get("collective").authors.values()
        author_ids = [int(a.get("id")) for a in authors]
        if (user.is_superuser is False and user.id not in author_ids):
            raise PermissionDenied
        if self.request.POST:
            data['addresses_form'] = AddressFormSet(self.request.POST, instance=self.object)
        else:
            data['addresses_form'] = AddressFormSet(instance=self.object)
        data['form'].fields['name'].widget.attrs['class'] = 'validate'
        data['form'].fields['name'].widget.attrs['oninvalid'] = "this.setCustomValidity('Bitte \
gib einen Namen des Kollektivs an!')"
        data['my_collectives'] = get_my_collectives(self.request)
        return data


class CollectiveCheckView(LoginRequiredMixin, CreateView):
    model = Checked
    template_name = 'collective_check.html'
    fields = ('result', )

    def setup(self, request, *args, **kwargs):
        setattr(request, "app_config", django_apps.get_app_config("collectives"))
        return super().setup(request, *args, **kwargs)

    def test_func(self):
        return self.request.user.is_staff

    def form_valid(self, form):
        form.instance.checker = self.request.user
        form.instance.collective = get_object_or_404(Collective, pk=self.kwargs['pk'])
        self.success_url = reverse_lazy('collective_detail', kwargs={'pk':self.kwargs['pk']})
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['collective'] = get_object_or_404(Collective, pk=self.kwargs['pk'])
        data['ddval'] = 'Prüfungsergebnis'
        data['form'].fields['result'].choices = [(cr.name, cr.name) for cr in CheckResultClass.objects.all()]
        return data


class CollectiveDeleteView(LoginRequiredMixin, UserPassesTestMixin,
                           DeleteView):
    model = Collective
    template_name = 'collective_delete.html'
    success_url = reverse_lazy('collective_list')

    def setup(self, request, *args, **kwargs):
        setattr(request, "app_config", django_apps.get_app_config("collectives"))
        return super().setup(request, *args, **kwargs)

    def test_func(self):
        return self.request.user.is_staff

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.delete()
        del request.session["my_collectives"]
        return HttpResponseRedirect(success_url)


class MapView(ListView):
    template_name = 'map.html'

    def setup(self, request, *args, **kwargs):
        setattr(request, "app_config", django_apps.get_app_config("collectives"))
        return super().setup(request, *args, **kwargs)

    def get_queryset(self):
        if self.request.user.is_superuser is True:
            queryset = Collective.objects.all()
        elif self.request.user.is_authenticated is False:
            queryset = Collective.objects.filter(public=True)
        else:
            queryset = Collective.objects.filter(
                Q(authors__id=self.request.user.id) | Q(public=True)
            )
        if self.request.GET.get('branche', None):
            queryset = queryset.filter(business=self.request.GET['branche'])
        if self.request.GET.get('region', None):
            if self.request.GET.get('region') == "keine":
                queryset = queryset.filter(postal_code="")
            else:
                postal_codes_in_state = self.get_postal_codes_for_state(self.request.GET['region'])
                queryset = queryset.filter(
                    postal_code__in=postal_codes_in_state
                )
        queryset = queryset.order_by(collate.asc())
        queryset = queryset.distinct()
        return queryset

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        return data
    
def get_collectives_coordinates(request):
    queryset = Collective.objects.filter(public=True)
    ret = {"features": []}
    for c in queryset:
        logo = None
        if c.logo:
            logo = c.logo.url
        kdksince = None
        kdkMember = KdKMember.objects.filter(collective=c).order_by('-since').first()
        if kdkMember is not None and kdkMember.till is None:
            kdksince = kdkMember.since
        business = ""
        for bc in c.businessClassList.all():
            if business != "":
                business += ", "
            business += bc.name
        additionalAdresses = AdditionalAddress.objects.filter(collective=c, current=True)
        for addr in additionalAdresses:
            if addr.lon is None:
                geodata = get_geodata_by_address(addr.address, addr.postal_code, addr.city)
                addr.lon = geodata.get("lon")
                addr.lat = geodata.get("lat")
                if addr.lon is not None:
                    addr.save()
                else:
                    continue
            ret["features"].append({
                "geometry": {
                    "coordinates": [
                        addr.lon,
                        addr.lat
                    ],
                    "type": "Point"
                },
                "properties": {
                    "zip": f"{addr.postal_code}",
                    "city": f"{addr.city}",
                    "address": addr.address,
                    "business": business,
                    "name": c.name,
                    "description": c.title,
                    "website": c.website,
                    "email": c.email,
                    "logo": logo,
                    "kdksince": kdksince,
                    "id": c.id,
                },
                "type": "Feature"
            })
             
    return JsonResponse(ret, status=200, safe=False)    
