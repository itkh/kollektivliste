import collectives.utils as utils
from django.test import TestCase


class UtilsTests(TestCase):

    def test_get_geodata_by_address_returns_dict_with_address_data(self):
        geodata = utils.get_geodata_by_address("Stresemannstraße 150",
                                               "Hamburg")
        assert isinstance(geodata, dict)
        assert "lat" in geodata.keys()
        assert "lon" in geodata.keys()
        assert geodata.get('lat').startswith('53.56')
        assert geodata.get('lon').startswith('9.9')

    def test_get_geodata_with_wrong_address_returns_empty_dict(self):
        geodata = utils.get_geodata_by_address("Iwewienfowien Straße 22",
                                               "Hamburg")
        assert isinstance(geodata, dict)
        assert geodata == {}

    def test_get_geodata_without_address_returns_empty_dict(self):
        geodata = utils.get_geodata_by_address("", "Hamburg")
        assert isinstance(geodata, dict)
        assert geodata == {}

    def test_get_geodata_without_city_returns_empty_dict(self):
        geodata = utils.get_geodata_by_address("Stresemannstraße 150", "")
        assert isinstance(geodata, dict)
        assert geodata == {}
