from CollectiveList.views import NewsletterPageView
from django.core import mail
from django.test import Client, RequestFactory, TestCase


class NewsletterViewTests(TestCase):

    def setUp(self):
        self.c = Client()

    def test_newsletter_view_get(self):
        resp = self.c.get("/newsletter/")
        self.assertEqual(resp.status_code, 200)
        content = resp.content.decode("utf-8")
        self.assertTrue("Abonniere unseren Newsletter" in content)
        self.assertFalse("Newsletter bestellt hast" in content)
        self.assertFalse("nicht abonniert" in content)

    def test_newsletter_view_post_has_success_message(self):
        with self.settings(NEWSLETTER_SUBSCRIBE_RECEIVER="user@example.local"):
            resp = self.c.post(
                "/newsletter/",
                {"sender": "vl@it.kollektiv.hamburg"}
            )
        send_email = mail.outbox[0]

        self.assertEqual(resp.status_code, 200)
        content = resp.content.decode("utf-8")
        self.assertTrue("Vielen Dank" in content)

        self.assertEqual(send_email.from_email, "vl@it.kollektiv.hamburg")
        self.assertEqual(send_email.to[0], "user@example.local")

    def test_newsletter_view_post_has_error_msg_if_sender_not_email(self):
        with self.settings(NEWSLETTER_SUBSCRIBE_RECEIVER="user@example.local"):
            resp = self.c.post(
                "/newsletter/",
                {"sender": "vlqkollektiv.hamburg"}
            )
        self.assertEqual(len(mail.outbox), 0)
        self.assertEqual(resp.status_code, 200)
        content = resp.content.decode("utf-8")
        self.assertTrue("Fehler aufgetreten" in content)
