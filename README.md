# Kollektivliste

Die Kollektivliste ist eine Webanwendung, die mit dem Django
Web Framework entwickelt wird. Für die Entwicklung der 
Kollektivliste verwenden wir Python >= 3.5.

Die Kollektivliste enthält eine Listen-, eine Detail- und Bearbeitungs-
Ansicht für Kollektivbetriebe. Zusätzlich eine Listen- und Bearbeitungs-
Ansicht für Veranstaltungen / Termine von Kollektivbetrieben. Dazu
einen Marktplatz mit Angeboten und Gesuchen von Kollektiven. Die Startseite
der Kollektivliste enthält jeweils eine Auswahlliste an Kollektiven,
den kommenden Terminen und aktuellen Angeboten und Gesuchen.

Neben den Angeboten auf der Webseite selbst, gehören noch ein Newsletter
und eine interne Mailingliste für Kollektivmitglieder zur Kollektivliste.
Für den Newsletter können sich Besucher/innen der Webseite per Formular
anmelden. Für die interne Mailingliste werden neue Benutzer nach dem
Speichern angemeldet und nach dem Löschen wieder abgemeldet.

## Installation für die Entwicklung

Wir empfehlen die Entwicklung in einer virtuellen Python
Entwicklungsumgebung. Dazu muss das Modul python3-venv auf deinem System
installiert sein (z.B. apt install python3-venv). Als Datenbank Backend
empfehlen wir PostgreSQL zu verwenden.

```bash
  $ git clone https://gitlab.com/itkh/kollektivliste.git
  $ cd kollektivliste
  $ python3 -m venv .
  $ source bin/activate
  $ pip install wheel
  $ pip install -r requirements.txt
```

pip installiert die neuesten Versionen der benötigten Pakete. Wenn du
stattdessen mit festgelegten Versionen der Pakete arbeiten willst, kannst du
anstelle der requirements.txt Datei die Datei requirements_versions.txt 
verwenden.

## Webserver starten

Den wsgi Server kann man ganz einfach mit folgendem Kommando starten:

```bash
  $ python manage.py runserver
```

Vor dem ersten Start des Servers sollte man noch die Anwendung einrichten
und die Datenbank mit den benötigten Tabellen erzeugen.


## Einrichtung

Vor dem ersten Start der Kollektivliste, muss man noch einige Einstellungen
vornehmen. So müssen z.B. alle notwendigen Informationen für die Datenbank
Verbindung vorhanden sein.

Diese Einstellungen kann man in der Datei settings.py vornehmen. Diese ist
nicht Teil des GIT Repositories. Du findest allerdings eine Vorlage zum
Kopieren im Repository. Du musst also zuerst die Datei sample-settings.py
zu settings.py kopieren und dann kannst du diese deinen Bedürfnissen
entsprechend anpassen:

```bash
  $ cp CollectiveList/sample-settings.py CollectiveList/settings.py
  $ vim CollectiveList/settings.py
```

Die Kollektivliste ist mit SQLite und PostgreSQL als mögliche Datenbank
Systeme entwickelt. Deshalb enthält die settings.py zwei DATABASES Bereiche.
Einmal für eine SQLite und einmal für eine PostgreSQL. Je nachdem welche
Datenbank du verwenden willst, kannst du einfach den nicht benötigten Bereich
entfernen und beim verbleibenden Bereich passenden Verbindungsdaten (vor allem
bei der PostgreSQL) eintragen.

Zum Versenden der Kontakt-Emails solltest du noch die Verbindungsdaten zu
einem SMTP Server eintragen (EMAIL_XXX Einträge). Über diesen SMTP Server 
werden dann die E-Mails versendet. Der Server kann lokal auf deinem Server
laufen oder entfernt bei einem anderen Anbieter.


## Datenbank verwalten

Wenn du die Datenbank deiner Wahl in die settings.py eingetragen hast, dann
musst du als erstes die Django interne Datenbank Migration laufen lassen.

```bash
  $ python manage.py migrate
```

Falls ansonsten keine Daten für die Datenbank vorhanden sind, kannst du nun
entweder einen Admin Benutzer anlegen (s.u.) oder den Server starten (s.o.).

Falls du Daten aus einer anderen Kollektivliste Installation importieren willst,
dann kannss du folgendes Django internes Kommando aufrufen:

```bash
  $ python manage.py loaddata data.json
```

Alternativ kannst du bei einer PostgreSQL Datenbank auch folgendes Kommando
verwenden:

```bash
  $ pg_restore -h localhost -U username -W -F t -d new_database_name database_dump_file.tar
```

Falls du zunächst Daten exportieren willst, um sie dann woanders zu importieren,
dann hast du folgendes Kommando zur Hand:

```bash
  $ python manage.py dumpdata > data.json
```

Auch hier alternativ für eine PostgreSQL Datenbank:

```bash
  $ pg_dump -h localhost -U username -W -F t database_name > database_dump_file.tar
```

Wenn du eines der Daten Model in deiner Django App geändert hast, dann musst
du eine neue Migration anlegen. Im ersten Schritt einfach das entsprechende
Model bearbeiten. Danach dann die Migration erstellen:

```bash
  $ python manage.py makemigrations
```

## Bilddateien verwalten

Die Kollektivliste enthält Bilddateien, die in ein media Verzeichnis
gespeichert werden. Dieses ist nicht Teil des GIT Repositories. Das Media
Verzeichnis in ein tar Archiv packen:

```bash
  $ tar -czf media.tar.gz ./media/
```

Media Verzeichnisse aus tar Archiv auspacken:

```bash
  $ tar -xzf media.tar.gz
```

## Einen Admin User einrichten

```bash
  $ python manage.py createsuperuser --email name@example.com
```

## Statische Dateien einbinden

Die statischen Dateien (js, css, fonts etc.) in ein static Verzeichnis
im Basis Verzeichnis kopieren:

```bash
  $ python manage.py collectstatic
```

## Anwendung aktualisieren (verwendete Pakete und Datenbank)

```bash
  $ pip install -r requirements.txt --upgrade
  $ python manage.py migrate
```

## Tests ausführen: Unit Tests und Integration Tests

Das Testing der Kollektivliste umfasst sowohl Python unit tests als
auch Cypress Integrationstests

### Unit Tests

Die Python Unittests werden gegen eine eigene Test-Datenbank ausgeführt.
Wenn du mit einer Postgresql Datenbank arbeitest, dann muss dein
Benutzer die Erlaubnis haben, neue Datenbanken anzulegen. Dies
geht mit folgendem psql Kommando:

```bash
  $ ALTER USER dein_user CREATEDB;
```

Bei einer SQLite Datenbank muss man keine besonderen Vorkehrungen
treffen.

Danach kann man die Tests ausführen:

```bash
  $ python manage.py test tests/kollektivliste_unit_tests/
```

### Integrationstests

Für die Integrationstests verwenden wir Cypress. Cypress und all seine
Abhängigkeiten werden beim ersten Aufruf automatisch installiert. Es
braucht zunächst einmal eine cypress.json Datei mit folgendem Inhalt:

```bash
  $ vim cypress.json
    {
      "baseUrl": "http://localhost:8000"
    }
```

Die localhost URL ist die übliche URL für eine lokale Installation. Sollen
die Cypress Tests gegen die produktive Kollektivliste Installation laufen,
dann muss in die Datei folgende baseURL eingetragen werden:
https://www.kollektivliste.org

```bash
  $ python manage.py testserver data.json
```

Die Integrationstests werden mit Cypress ausgeführt:

```bash
  $ npm run cypress:open
```
 ## Produktivbetrieb

 ### Produktiv-App neu starten (Hostsharing: Phusion Passenger)

```bash
  $ passenger-config restart-app
```
